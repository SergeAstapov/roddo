<?php

namespace Roddo\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;


class DefaultController extends Controller
{
    
    public function indexAction($name)
    {
        return $this->render('RoddoClientBundle:Default:index.html.twig', array('name' => $name));
    }
}
