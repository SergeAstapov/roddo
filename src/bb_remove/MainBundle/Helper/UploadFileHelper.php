<?php

namespace Roddo\MainBundle\Helper;

use Symfony\Component\HttpFoundation\File\File,
	Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * 
 */
class UploadFileHelper
{
	private $file;
	private $prev_file;
	private $root_dir;
	private $secure_basename;
	private $ext_whitelist = array(
		'jpg jpeg gif png txt html doc xls pdf ppt pps odt ods odp'
	);

	public function __construct() {
		
	}

	public function setFile(File $file) {
		if (!empty($file)) {
			if (!empty($this->file)) {
				$this->prev_file = $this->file;
			}

			$this->file = $file;
			unset($this->secure_basename);
		}
	}

	public function setAjaxFile($filepath) {
		$file = new File($this->root_dir . DIRECTORY_SEPARATOR . $filepath, FALSE);

		if (!empty($file) && $file->isReadable()) {
			$this->file = $file;
		}
	}

	public function setRootDir($directory) {
		$this->root_dir = rtrim($directory, DIRECTORY_SEPARATOR);
	}

	public function setUploadDir($directory) {
		$this->upload_dir = ltrim($directory, DIRECTORY_SEPARATOR);
	}

	public function upload() {
		if (!empty($this->file)) {
			$this->removePrevFile();

			return $this->file->move(
				$this->getUploadRootDir(),
				$this->getSecureFilepath()
			);
		}
	}

	private function removePrevFile() {
		if (!empty($this->prev_file)) {
			$filesystem = new Filesystem();
			$filesystem->remove($this->prev_file);
		}
	}

	public function getSecureFilepath() {
		if (!empty($this->file)) {
			return $this->getUploadDir() . DIRECTORY_SEPARATOR . $this->getSecureBasename();
		}
	}

	public function getSecureBasename() {
		if (!empty($this->secure_basename)) {
			return $this->secure_basename;
		}

		if ($this->file instanceof UploadedFile) {
			$this->secure_basename = $this->generateSecureBasename();
			return $this->secure_basename;
		}
		else {
			$this->secure_basename = $this->file->getBasename();
			return $this->secure_basename;
		}
	}

	private function generateSecureBasename() {
		$filename = $this->file->getClientOriginalName();

		// Split the filename up by periods. The first part becomes the basename
		// the last part the final extension.
		$filename_parts = explode('.', $filename);
		$new_filename = array_shift($filename_parts); // Remove file basename.
		$final_extension = array_pop($filename_parts); // Remove final extension.

		// Loop through the middle parts of the name and add an underscore to the
		// end of each section that could be a file extension but isn't in the list
		// of allowed extensions.
		foreach ($filename_parts as $filename_part) {
			$new_filename .= '.' . $filename_part;
			if (!in_array($filename_part, $this->ext_whitelist) && preg_match("/^[a-zA-Z]{2,5}\d?$/", $filename_part)) {
				$new_filename .= '_';
			}
		}
		$filename = $new_filename . '.' . strtolower($final_extension);

		if (preg_match('/\.(php|pl|py|cgi|asp|js)(\.|$)/i', $filename) && (substr($filename, -4) != '.txt')) {
			$filename .= '.txt';
		}

		// Strip control characters (ASCII value < 32). Though these are allowed in
		// some filesystems, not many applications handle them well.
		$basename = preg_replace('/[\x00-\x1F]/u', '_', $filename);

		if (substr(PHP_OS, 0, 3) == 'WIN') {
			// These characters are not allowed in Windows filenames
			$basename = str_replace(array(':', '*', '?', '"', '<', '>', '|'), '_', $basename);
		}

		$destination = $this->getUploadDir() . DIRECTORY_SEPARATOR . $basename;

		if (file_exists($destination)) {
			// Destination file already exists, generate an alternative.
			$pos = strrpos($basename, '.');
			if ($pos !== FALSE) {
				$name = substr($basename, 0, $pos);
				$ext = substr($basename, $pos);
			}
			else {
				$name = $basename;
				$ext = '';
			}

			$counter = 0;
			do {
				$destination = $this->getUploadDir() . DIRECTORY_SEPARATOR . $name . '_' . $counter++ . $ext;
			} while (file_exists($destination));
		}

		return basename($destination);
	}

	private function getUploadDir() {
		return $this->upload_dir;
	}

	private function getUploadRootDir() {
		return $this->root_dir . DIRECTORY_SEPARATOR . $this->getUploadDir();
	}
}