<?php
namespace Roddo\ConnectBundle\Form;

use Symfony\Component\Form\FormBuilder;
use FOS\UserBundle\Form\Type\RegistrationFormType as BaseType;

class RegistrationFormType extends BaseType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
        $builder
            ->add('username')
            ->add('email', 'email')
            ->add('plainPassword', 'repeated', array(
				'type'        => 'password',
				'first_name'  => 'password',
				'second_name' => 'confirm_password',
			))
		;
    }

    public function getName()
    {
        return 'roddo_registration';
    }
}