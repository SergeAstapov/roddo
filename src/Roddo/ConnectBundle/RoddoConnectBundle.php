<?php

namespace Roddo\ConnectBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RoddoConnectBundle extends Bundle
{
	public function getParent()
	{
		return 'HWIOAuthBundle';
	}
}