<?php
namespace Roddo\EstimateBundle;

final class EstimateEvents
{
    /**
     * The estimate.pre_update event is thrown each time an estimate is
	 * ready to be saved in database.
     *
     * The event listener receives an Roddo\EstimateBundle\Event\EstimateUpdateEvent
     * instance.
     *
     * @var string
     */
    const PRE_UPDATE = 'estimate.pre_update';

    /**
     * The estimate.post_update event is thrown each time an estimate has been
	 * saved in database.
     *
     * The event listener receives an Roddo\EstimateBundle\Event\EstimateUpdateEvent
     * instance.
     *
     * @var string
     */
    const POST_UPDATE = 'estimate.post_update';
}