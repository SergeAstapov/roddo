<?php
namespace Roddo\EstimateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use JMS\SecurityExtraBundle\Annotation\Secure,
	JMS\SecurityExtraBundle\Annotation\PreAuthorize;

use Roddo\EstimateBundle\Form\UserSettingsType;

/**
 * Estimate controller.
 *
 */
class UserSettingsController extends Controller
{
    /**
	 * @Secure(roles="ROLE_USER")
	 * @Template
     */
	public function editAction()
	{
		$user = $this->get('security.context')->getToken()->getUser();
		$form = $this->createForm(new UserSettingsType(), $user);

		$request = $this->getRequest();

		if ($request->getMethod() == 'POST') {
			$form->bindRequest($request);

			if ($form->isValid()) {
				$userProvider = $this->container->get('fos_user.user_manager');
				$userProvider->updateUser($user);

				$this->get('session')->setFlash('message', 'Settings have been updated.');

				return $this->redirect($this->generateUrl('user_settings'));
			}
		}

        return array(
            'user' => $user,
            'form' => $form->createView(),
        );
	}
}