<?php

namespace Roddo\EstimateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
	Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;


class DefaultController extends Controller
{
	/**
	 * Application front page.
	 *
	 * @Template
	 */
	public function indexAction() { return array(); }

	/**
	 * Application help page.
	 *
	 * @Template
	 */
	public function helpAction() { return array(); }
}