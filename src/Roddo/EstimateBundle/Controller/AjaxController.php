<?php

namespace Roddo\EstimateBundle\Controller;

use Roddo\MainBundle\Helper\UploadFileHelper;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
	Symfony\Component\HttpFoundation\Response,
	Symfony\Component\Validator\Constraints\Image;


class AjaxController extends Controller
{
	/**
	 * Upload files via AJAX and store them in temprorary location.
	 */
	public function uploadAction() {
		$request = $this->getRequest();
		$result = array(
			'status'   => 'success',
			'messages' => array(),
			'filepath' => '',
		);

		if (!$request->isXMLHttpRequest()) {
			throw $this->createNotFoundException('Uploader not found.');
		}

		$file = $request->files->get('ajax_file');

		$validator = $this->get('validator');
		$violations = $validator->validateValue($file, new Image());

		// Are there any errors with uploaded file?
		if ($violations->count() > 0) {
			foreach ($violations as $violation) {
				$result['messages'][] = $violation->getMessage();
			}
		}

		// Hooray! Move file to temporary destination.

		$fileHelper = new UploadFileHelper();
		$fileHelper->setRootDir($this->container->getParameter('kernel.root_dir') . '/../web');
		$fileHelper->setUploadDir($this->container->getParameter('roddo_estimate.media.tmp_dir'));
		$fileHelper->setFile($file);
		$fileHelper->upload();

		$result['filepath'] = $fileHelper->getSecureFilepath();

		return new Response(json_encode($result), 200, array('Content-Type' => 'application/json'));
	}
}