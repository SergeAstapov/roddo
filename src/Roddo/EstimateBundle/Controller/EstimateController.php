<?php

namespace Roddo\EstimateBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller,
	Symfony\Component\HttpFoundation\Response,
	Sensio\Bundle\FrameworkExtraBundle\Configuration\Template,
	JMS\SecurityExtraBundle\Annotation\Secure,
	JMS\SecurityExtraBundle\Annotation\PreAuthorize;

use Roddo\EstimateBundle\EstimateEvents,
	Roddo\EstimateBundle\Event\EstimateUpdateEvent,
	Roddo\EstimateBundle\Entity\Estimate,
	Roddo\EstimateBundle\Entity\EstimateAttribute,
	Roddo\EstimateBundle\Form\EstimateType;

use Roddo\EstimateBundle\Form\EstimateEmailType,
	Roddo\EstimateBundle\Entity\EstimateEmail;

/**
 * Estimate controller.
 *
 */
class EstimateController extends Controller
{
	/**
	 * Lists all Estimate entities created by currently logged in user.
	 *
	 * @Secure(roles="ROLE_USER")
	 * @Template
	 */
	public function indexAction()
	{
		$user = $this->get('security.context')->getToken()->getUser();

		$em = $this->getDoctrine()->getEntityManager();

		$entities = $em->getRepository('RoddoEstimateBundle:Estimate')->findAllByAuthor($user->getId());

		return array('entities' => $entities);
	}

	/**
	 * Creates a new Estimate entity.
	 *
	 * @Template
	 */
	public function createAction()
	{
		$request  = $this->getRequest();
		$estimate = $this->createEstimate();
		$form     = $this->createForm(new EstimateType(), $estimate);

		if ($request->getMethod() == 'POST') {
			$form->bindRequest($request);

			if ($form->isValid()) {
				$em = $this->getDoctrine()->getEntityManager();
				$this->saveEstimate($estimate, $em);

				$this->get('session')->setFlash('notice', 'Estimate has been created. Hooray!');

				return $this->redirect(
					$this->generateUrl('estimate_show', array('id' => $estimate->getId()))
				);
			}
		}

		return array(
			'estimate' => $estimate,
			'form'     => $form->createView(),
		);
	}

	/**
	 * Finds and displays a Estimate entity.
	 *
	 * @Template
	 */
	public function showAction($id)
	{
		$estimate = $this->getDoctrine()
			->getEntityManager()
			->getRepository('RoddoEstimateBundle:Estimate')->find($id);

		if (is_null($estimate)) {
			throw $this->createNotFoundException('Unable to find Estimate entity.');
		}

		$attributes = array();
		foreach ($estimate->getAttributes() as $attribute) {
			$attributes[$attribute->getAttribute()] = $attribute->getValue();
		}

		$deleteForm = $this->createDeleteForm($id);

		return array(
			'estimate'    => $estimate,
			'items'       => $estimate->getItems(),
			'attributes'  => $attributes,
			'delete_form' => $deleteForm->createView(),
		);
	}

	/**
	 * Displays a form to edit an existing Estimate entity.
	 *
	 * @Template
	 */
	// @PreAuthorize("hasRole('ADMIN') or #toDelete.getId() == user.getId()")
	public function editAction($id)
	{
		$em = $this->getDoctrine()->getEntityManager();
		$estimate = $em->getRepository('RoddoEstimateBundle:Estimate')->find($id);

		if (is_null($estimate)) {
			throw $this->createNotFoundException('Unable to find Estimate entity.');
		}

		// Be sure that that there is always al least one empty row in estimate form.
		$estimate->addEmptyItem();

		$editForm   = $this->createForm(new EstimateType(), $estimate);
		$deleteForm = $this->createDeleteForm($id);

		$request = $this->getRequest();

		if ($request->getMethod() == 'POST') {
			$editForm->bindRequest($request);

			if ($editForm->isValid()) {
				$this->saveEstimate($estimate, $em);

				$this->get('session')->setFlash('message', 'Estimate has been updated.');

				return $this->redirect($this->generateUrl('estimate_show', array('id' => $id)));
			}
		}

		return array(
			'estimate'    => $estimate,
			'form'        => $editForm->createView(),
			'delete_form' => $deleteForm->createView(),
		);
	}

	/**
	 * Deletes an Estimate entity.
	 *
	 */
	public function deleteAction($id)
	{
		$form    = $this->createDeleteForm($id);
		$request = $this->getRequest();

		$form->bindRequest($request);

		if ($form->isValid()) {
			$em = $this->getDoctrine()->getEntityManager();
			$entity = $em->getRepository('RoddoEstimateBundle:Estimate')->find($id);

			if (!$entity) {
				throw $this->createNotFoundException('Unable to find Estimate entity.');
			}

			$em->remove($entity);
			$em->flush();
		}

		return $this->redirect($this->generateUrl('frontpage'));
	}

	/**
	 * Send an Estimate via e-mail.
	 * 
	 * @Template
	 */
	public function sendAction($id)
	{
		$em = $this->getDoctrine()->getEntityManager();

		$estimate = $em->getRepository('RoddoEstimateBundle:Estimate')->find($id);
		if (!$estimate) {
			throw $this->createNotFoundException('Unable to find Estimate entity.');
		}

		$estimateEmail = new EstimateEmail();
		$form = $this->createForm(new EstimateEmailType(), $estimateEmail);

		$request = $this->getRequest();

		if ($request->getMethod() == 'POST') {
			$form->bindRequest($request);

			if ($form->isValid()) {
				$message = \Swift_Message::newInstance()
					->setFrom($this->container->getParameter('mailer_from'))
					->setTo($estimateEmail->getEmail())
					->setSubject($estimateEmail->getSubject())
					->setBody($estimateEmail->getBody())
	//					->setBody($this->renderView('HelloBundle:Hello:email.txt.twig', array('name' => $name)))
				;
				$this->get('mailer')->send($message);

				$this->get('session')->setFlash('message', 'Estimate has been send successfully to '. $estimateEmail->getEmail());

				return $this->redirect(
					$this->generateUrl('estimate_show', array('id' => $id))
				);
			}
		}

		return array(
			'estimate' => $estimate,
			'form'     => $form->createView(),
		);
	}

	/**
	 * Download Estimate entity.
	 */
	public function downloadAction($id)
	{
		$em = $this->getDoctrine()->getEntityManager();

		$estimate = $em->getRepository('RoddoEstimateBundle:Estimate')->find($id);
		if (!$estimate) {
			throw $this->createNotFoundException('Unable to find Estimate entity.');
		}

		$pdfObj = $this->container->get("white_october.tcpdf")->create();

		// set document information
		$pdfObj->SetCreator('Roddo Estimates');
		$pdfObj->SetAuthor('Nicola Asuni');
		$pdfObj->SetTitle('TCPDF Example 006');
		$pdfObj->SetSubject('TCPDF Tutorial');
		$pdfObj->SetKeywords('TCPDF, PDF, example, test, guide');

		$pdfObj->SetFont('dejavusans', '', 10);

		$attributes = array();
		foreach ($estimate->getAttributes() as $attribute) {
			$attributes[$attribute->getAttribute()] = $attribute->getValue();
		}

		$html = $this->container
			->get('templating')
			->render('RoddoEstimateBundle:Estimate:show.pdf.twig', array(
				'estimate'    => $estimate,
				'items'       => $estimate->getItems(),
				'attributes'  => $attributes,
			)); //'Some HTML!';

		$pdfObj->AddPage();
		// output the HTML content
		$pdfObj->writeHTML($html, true, false, true, false, '');
//$pdf->lastPage();
		
		return new Response($pdfObj->Output('void', 'S'), 200, array(
			'Content-Type'        => 'application/pdf',
			'Content-Disposition' => 'attachment; filename="downloaded.pdf"'
		));
	}

	private function createDeleteForm($id)
	{
		return $this->createFormBuilder(array('id' => $id))
			->add('id', 'hidden')
			->getForm()
		;
	}

	/**
	 * 
	 * @return \Roddo\EstimateBundle\Entity\Estimate
	 */
	private function createEstimate() {
		$estimate = new Estimate();

		// Be sure that that there is always al least one empty row in estimate form.
		$estimate->addEmptyItem();

		if ($this->get('security.context')->isGranted('IS_AUTHENTICATED_FULLY')) {
			$user = $this->get('security.context')->getToken()->getUser();
			$estimate->setUser($user);

			$mapping = array(
				'Fullname'     => 'author_name',
				'Phone'        => 'author_phone',
				'CompanyName'  => 'author_company',
				'CompanyAbout' => 'author_about',
			);

			foreach ($mapping as $userProperty => $attribute_name) {
				$userGetter = 'get' . $userProperty;

				if (null != ($value = call_user_func(array($user, $userGetter)))) {
					$attr = new EstimateAttribute();
					$attr->setAttribute($attribute_name);
					$attr->setEstimate($estimate);
					$attr->setValue($value);
					$estimate->addAttribute($attr);
				}
			}
		}

		return $estimate;
	}

	/**
	 * 
	 * @param \Roddo\EstimateBundle\Entity\Estimate $estimate
	 * @param \Doctrine\ORM\EntityManager $em
	 * @throws \Exception
	 */
	private function saveEstimate(Estimate $estimate, \Doctrine\ORM\EntityManager $em) {
		$dispatcher = $this->container->get('event_dispatcher');

		$em->getConnection()->beginTransaction();
		try {
			/**
			 * Create the EstimateUpdateEvent and dispatch it.
			 * It differs from doctrine.event_listener in way that we save
			 * estimate attributes separately from eastimate. But thits event
			 * contains full estimate object.
			 */
			$preUpdateEvent = new EstimateUpdateEvent($estimate);
			$dispatcher->dispatch(EstimateEvents::PRE_UPDATE, $preUpdateEvent);


			/**
			 * Save attributes after Estimate Entity is saved, because we
			 * need estimate_id to be presented within each EstimateAttribute
			 * object since estimate_id is part of composite primary_key.
			 */
			$attributes = $estimate->getAttributes();
			$estimate->setAttributes(new \Doctrine\Common\Collections\ArrayCollection());

			$em->persist($estimate);
			$em->flush();

			foreach ($attributes as $attribute) {
				if ($attribute->getValue()) {
					$em->persist($attribute);
				}
			}
			$em->flush();

			$estimate->setAttributes($attributes);

			
			/**
			 * Create the EstimateUpdateEvent and dispatch it.
			 * It differs from doctrine.event_listener in way that we save
			 * estimate attributes separately from eastimate. But thits event
			 * contains full estimate object.
			 */
			$postUpdateEvent = new EstimateUpdateEvent($estimate);
			$dispatcher->dispatch(EstimateEvents::POST_UPDATE, $postUpdateEvent);


			$em->getConnection()->commit();
		}
		catch (\Exception $e) {
			$em->getConnection()->rollback();
//			$em->close();
			throw $e;
		}
	}
}