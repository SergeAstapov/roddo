<?php
namespace Roddo\EstimateBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EstimateItemType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder
			->add('description', 'textarea', array('required' => false, 'attr' => array('rows' => 1)))
			->add('quantity',    'text',     array('required' => false))
			->add('price',       'text',     array('required' => false))
			->add('delta',       'hidden',   array('required' => false))
//			->add('item_id', 'hidden')
		;
	}

	public function getName()
	{
		return 'estimate_item';
	}

	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class' => 'Roddo\EstimateBundle\Entity\EstimateItem',
		);
	}
}