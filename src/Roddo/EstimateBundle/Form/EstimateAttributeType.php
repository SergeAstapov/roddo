<?php
namespace Roddo\EstimateBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EstimateAttributeType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$options = array('required' => false);

		$item_type = 'text';
		switch ($builder->getName()) {
			case 'city'           : $options['label'] = 'City';    break;
			case 'client_name'    : $options['label'] = 'Client name';        break;
			case 'client_company' : $options['label'] = 'Client company';     break;
			case 'author_name'    : $options['label'] = 'Your name';          break;
			case 'author_phone'   : $options['label'] = 'Your phone';         break;
			case 'author_company' : $options['label'] = 'Your company name';  break;
			case 'author_about'   :
				$options['label'] = 'About your company';
				$item_type = 'textarea';
				break;
			case 'conditions'     :
				$options['label'] = 'Conditions';
				$item_type = 'textarea';
				break;

			case 'date' :
//				$options['format'] = 'yyyy-MM-dd';
//				$options['widget'] = 'single_text';
				$options += array(
					'label' => 'Date',
					'help'  => 'Format: YYYY-MM-DD',
					'invalid_message' => 'You entered an invalid date - it should have the YYYY-MM-DD format',
				);
				break;

			case 'logo' :
				$item_type = 'file';
				$builder->add('ajax_file', 'hidden', array('property_path' => FALSE));
				break;
		}

		$builder->add('value', $item_type, $options);
	}

	public function getName()
	{
		return 'estimate_attribute';
	}

	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class' => 'Roddo\EstimateBundle\Entity\EstimateAttribute',
		);
	}
}