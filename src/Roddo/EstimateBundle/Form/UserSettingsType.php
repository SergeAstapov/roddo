<?php
namespace Roddo\EstimateBundle\Form;

use Symfony\Component\Form\AbstractType,
	Symfony\Component\Form\FormBuilder;

class UserSettingsType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder->setRequired(FALSE);

		$builder
			->add('fullname', 'text')
			->add('phone', 'text')
			->add('company_name', 'text')
			->add('company_about', 'textarea')
		;
	}

    public function getName() {
		return 'user_settings';
	}

	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class'      => 'Roddo\EstimateBundle\Entity\User',
			'csrf_protection' => TRUE,
			'csrf_field_name' => '_token',
			'intention'       => $this->getName(),
		);
	}
	
	public function setDefaultOptions(OptionsResolverInterface $resolver)
	{
		$resolver->setDefaults(array(
			'validation_groups' => array('user_settings')
		));
	}
}