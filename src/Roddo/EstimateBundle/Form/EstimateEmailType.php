<?php
namespace Roddo\EstimateBundle\Form;

use Symfony\Component\Form\AbstractType,
	Symfony\Component\Form\FormBuilder;

class EstimateEmailType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
    {
		$builder->setRequired(FALSE);

        $builder->add('email', 'email');
        $builder->add('subject');
        $builder->add('body', 'textarea');
    }

    public function getName()
    {
        return 'estimate_send';
    }
}