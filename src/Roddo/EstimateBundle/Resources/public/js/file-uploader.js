;(function ( $, window, undefined ) {
	// Create the defaults once
	var pluginName = 'RoddoFileUploader',
		document = window.document,
		defaults = {
			allowedFileTypes: [
				'image/jpeg',
				'image/png',
				'image/gif',
			],
			maxSize: 1024*1024, // 1Mb
			imageResize: false,
			uploadPath : 'ajax-upload',
			msg: {
				dropZone: 'Drop file here to upload',
				errorSize: 'File `%filename%` is to big to upload. Max is size is %max_size%',
				errorType: 'File `%filename%` is not allowed to upload.',
				errorUpload: 'Error uploading file to server server'
			}
		};

	function Plugin( element, options ) {
		this.element = element;

		this.options = $.extend( {}, defaults, options) ;

		this._defaults = defaults;
		this._name = pluginName;

		this.fileAPIsupported = typeof(window.XMLHttpRequest) != 'undefined' &&
								typeof(window.FileReader)     != 'undefined' &&
								typeof(window.File)           != 'undefined' &&
								typeof(window.FormData)       != 'undefined';

		this.init();
	};

	Plugin.prototype.init = function () {
		// Place initialization logic here
		// You already have access to the DOM element and the options via the instance, 
		// e.g., this.element and this.options
		$(this.element).hide().before( this.templateUploader() );
		

		this.initDragNDropUpload();
		this.initFileSelect();
	};

	Plugin.prototype.templateUploader = function() {
		var $div = $('<div class="RoddoFileUploader" />');

		if (this.fileAPIsupported) {
			this.dropZone = $('<div class="RoddoDropZone" />').text( this.options.msg.dropZone);

			$div.append(this.dropZone);
			$div.append( $('<span class="RoddoUploadMethodSwitcher" />').text('or') );
		}

		this.fileSelector = $('<div class="RoddoFileSelector" />').text('Choose file');
		this.fileNameDiv  = $('<div class="RoddoFileName" />');

		$div.append(this.fileSelector);
		$div.append(this.fileNameDiv );

		return $div;
	};

	Plugin.prototype.initDragNDropUpload = function() {
		var self = this;

		var eventCancel = function(event) {
			event.stopPropagation();
			event.preventDefault();
		};

		/**
		 * Bind empty events that prevents browser from using Drag'n'Drop.
		 */
		var eventsCancel = ['dragenter', 'dragover', 'dragleave', 'dragexit', 'drop'];
		for (var i in eventsCancel) {
			this.dropZone.on(eventsCancel[i], eventCancel);
		}


		this.dropZone.on({
			dragover: function(e) {
				$(this).addClass('hover');

				var dataTransfer = e.originalEvent.dataTransfer;
				if (dataTransfer) {
					dataTransfer.dropEffect = 'copy';
				}
			},
			dragleave: function() { $(this).removeClass('hover') },
			drop: function(e) {
				$(this).removeClass('hover');

				var dataTransfer = e.originalEvent.dataTransfer;
				if (dataTransfer && dataTransfer.files.length) {
					var file = dataTransfer.files[0];

					if (self.isFileAllowed(file)) {
						self.setFilename(file);
						self.showThumbnail(file);
						self.uploadProcess(file);
					}
				}
			}
		});
	};

	Plugin.prototype.initFileSelect = function() {
		var self = this;
		var $input = $('input[type="file"]', this.element).add(this.element).filter('input[type="file"]');

		$(this.fileSelector).click(function(e) {
			e.preventDefault();

			$input.trigger('click');
		});

		$input.change(function(e) {
			var file = {
				name: $(this).val(),
				size: 0
			};

			// Use File API to access selected file.
			if (this.files && this.files.length) {
				file = this.files[0];
			}

			if (self.isFileAllowed(file)) {
				self.setFilename(file);

				if (self.fileAPIsupported) {
					self.showThumbnail(file);
				}
			}
		});
	}

	Plugin.prototype.isFileAllowed = function(file) {
		var allowed = false,
			errorSize = (this.options.msg.errorSize).replace('%filename%', file.name),
			errorType = (this.options.msg.errorType).replace('%filename%', file.name);

		errorSize = errorSize.replace('%max_size%', this.formatSize(this.options.maxSize));

		for (var i in this.options.allowedFileTypes) {
			if (this.options.allowedFileTypes[i] === file.type) {
				allowed = true;
				break;
			}
		}

		if (!allowed && $.showMessage) {
			$.showMessage({
				thisMessage: [errorType],
				useEsc     : false,
				className  : 'error',
				autoClose  : true
			});
		}

		if (allowed && file.size > this.options.maxSize) {
			$.showMessage({
				thisMessage: [errorSize],
				useEsc     : false,
				className  : 'error',
				autoClose  : true
			});
			allowed = false;
		}

		return allowed;
	}

	Plugin.prototype.showThumbnail = function(file) {
		var $thumb = jQuery('<div class="RoddoFilePreview" />');

		var reader = new FileReader();
		reader.onload = function(e) {
			$thumb.css('background-image', 'url("'+ e.target.result +'")');
		};
		reader.readAsDataURL(file);

		this.dropZone.children('.RoddoFilePreview').remove();
		this.dropZone.append($thumb);
	}

	Plugin.prototype.setFilename = function(file) {
		this.fileNameDiv.text(
			file.name +' ('+ this.formatSize(file.size) +')'
		);
	}

	Plugin.prototype.formatSize = function(size) {
		var sizeExt = ['b', 'Kb', 'Mb', 'Gb'], i = 0

		while (size > 1024) {
			i++;
			size = size / 1024;
		}

		if (parseInt(size, 10) != size) {
			size = size.toFixed(2);
		}

		return size + sizeExt[i];
	}

	Plugin.prototype.uploadProcess = function(file) {
		var self = this;

		var uploadProgress = function(event) {
			if (event.lengthComputable) {
				self.uploadUpdateProgress.call(self, (event.loaded * 100) / event.total);
			}
		};

		var uploadStateChange = function(event) {
			if (this.readyState == 4) { // The AJAX operation is complete.
				var result = $.parseJSON(this.responseText);
				self.options.ajaxFile.val('');

				if (result['status'] === 'success') {
					self.options.ajaxFile.val( result['filepath'] );
				}
				else {
					$.showMessage({
						thisMessage: result['messages'],
						useEsc     : false,
						className  : 'error',
						autoClose  : false
					});
				}
			}
		};

		var uploadDone = function(event) {
//			console.log(event)
		};

		var uploadError = function(event) {
			$.showMessage({
				thisMessage: [self.options.msg.errorUpload],
				useEsc     : false,
				className  : 'error',
				autoClose  : true
			});
		};

		var xhr = new XMLHttpRequest();
		xhr.upload.addEventListener('progress', uploadProgress, false);
		xhr.upload.addEventListener('load',     uploadDone,     false);
		xhr.upload.addEventListener('error',    uploadError,    false);
		xhr.onreadystatechange = uploadStateChange;

		xhr.open('POST', RoddoApp.config.basePath + this.options.uploadPath);
		xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
		xhr.setRequestHeader('Accept', 'application/json');

		var fd = new FormData();
		fd.append('ajax_file', file);
		// Initiate a multipart/form-data upload
		xhr.send(fd);

		if (xhr.status == 200) {
			alert(xhr.responseText);
		}
	}

	Plugin.prototype.uploadUpdateProgress = function(progress) {
		
	}

	// A really lightweight plugin wrapper around the constructor, 
	// preventing against multiple instantiations
	$.fn[pluginName] = function ( options ) {
		return this.each(function () {
			if (!$.data(this, 'plugin_' + pluginName)) {
				$.data(this, 'plugin_' + pluginName, new Plugin( this, options ));
			}
		});
	}
}(jQuery, window));



//RoddoFileUploader = function(options) {
//	var self = this;
//	self.options    = options;
//	self.fileReader = typeof(window.FileReader) != 'undefined';
//
//	self.setupFileUploader();
////	if (typeof(window.FileReader) != 'undefined') {
////		self.bindFileDrop();
////	}
//}
//
//RoddoFileUploader.prototype.setupFileUploader = function() {
//	var self = this;
//
////	self.options.
//}
////<div class="js-uploader">
////			<div id="logoDropZone">Drop logo here to upload</div>
////			<span class="method-select">or</span>
////			<button id="logoFileSelector">Choose file</button>
////		</div>
////		<div class="basic-uploader">
//
//RoddoFileUploader.prototype.log = function(str) {
//	if (console && console.log) {
//		console.log(str);
//	}
//}
//
//RoddoFileUploader.prototype.showMessage = function(type, message, autoClose) {
//	if ($.showMessage) {
//		if (type != 'error' && type != 'warning') {
//			type = 'notification';
//		}
//
//		$('body').showMessage({
//			thisMessage: [message],
//			useEsc     : false,
//			className  : type,
//			autoClose  : autoClose || true,
//			delayTime  : 2000
//		});
//	}
//}
//
//RoddoFileUploader.prototype.bindFileDrop = function() {
//	var self = this,
//		dropZone = self.options.dropZone || null;
//
//	if (dropZone) {
//		dropZone.parent().addClass('dragndrop');
//
//		/**
//		 * Bind empty events that prevents browser from using Drag'n'Drop.
//		 */
//		dropZone.on({
//			dragenter: self.eventCancel,
//			dragover : self.eventCancel,
//			dragleave: self.eventCancel,
//			dragexit : self.eventCancel,
//			drop     : self.eventCancel
//		});
//
//
//
//		dropZone.on({
//			dragover : function(e) {
//				dropZone.addClass('hover');
//
//				var dataTransfer = e.originalEvent.dataTransfer;
//				if (dataTransfer) {
//					dataTransfer.dropEffect = 'copy';
//				}
//			},
//			dragleave: function() { dropZone.removeClass('hover') },
//			drop: function(e) {
//				dropZone.removeClass('hover')
//
//				self.log('Catch drop event.');
//
//				var dataTransfer = e.originalEvent.dataTransfer;
//				if (dataTransfer && dataTransfer.files.length) {
//					self.file = dataTransfer.files[0];
//
//					if (self.fileAllowed()) {
//						self.log('Dropped '+ dataTransfer.files.length +' files.');
//
//						self.showThumbnail();
//						self.upload();
//					}
//				}
//			}
//		});
//	}
//}
//
//RoddoFileUploader.prototype.eventCancel = function(event) {
//	event.stopPropagation();
//	event.preventDefault();
//}
//
//RoddoFileUploader.prototype.fileAllowed = function() {
//	var self = this,
//		allowed = false,
//		errorMessage = 'File `'+ self.file.name +'` is not allowed to upload.';
//
//	for (var i in self.options.fileTypes) {
//		if (self.options.fileTypes[i] == self.file.type) {
//			allowed = true;
//			break;
//		}
//	}
//
//	var allowedTypes = 'Allowed file type: '+ self.options.fileTypes.join(', ');
//	switch (allowed) {
//		case true : self.log('File `'+ self.file.type +'` is allowed to upload. '+ allowedTypes); break;
//		case false: self.log('File `'+ self.file.type +'` is not allowed to upload. '+ allowedTypes); break;
//	}
//
//	if (!allowed) {
//		self.showMessage('error', errorMessage);
//	}
//
//	return allowed;
//}
//
//RoddoFileUploader.prototype.showThumbnail = function() {
//	var self = this;
//
//	self.log('Preparing thumbnail for `'+ self.file.name +'` (type '+ self.file.type +')');
//
//	var $thumb = jQuery('<img />');
//
//	// Создаем объект FileReader и по завершении чтения файла, отображаем миниатюру и обновляем
//	// инфу обо всех файлах
//	var reader = new FileReader();
//	reader.onload = (function(aIcon) {
//		return function(e) {
//			if (isImage) {
//				aIcon.attr('src', e.target.result);
//				aIcon.attr('width', 150);
//			}
//			var fileSize = Math.round(file.size / 1024);
//			var fileName = aIcon.prev('.file-name');
//			fileName.text(fileName.text() + ' ('+ fileSize +' Кб)');
//
//			log('Картинка добавлена: `'+ file.name + '` ('+ fileSize +' Кб)');
//
//			filesCount++;
//			filesSize += file.size;
//			updateInfo();
//		};
//	})(icon);
//
//	reader.readAsDataURL(self.file);
//}
//
//RoddoFileUploader.prototype.upload = function() {
//	var self = this;
//	
//}
//function RoddoLogoUpload() {
//	var dropZone = $('#logoDropZone'),
//		maxSize = 1024*1024; // максимальный размер файла - 1 мб.
//
//	if (typeof(window.FileReader) == 'undefined') {
//		dropZone.closest('.section-logo').addClass('unsupported');
//		return;
//	}
//
//	var eventCancel = function(e) {
//		e.stopPropagation();
//		e.preventDefault();
//	};
//	dropZone.on({
//		dragover : function(e) { dropZone.addClass('hover')    ; eventCancel(e) },
//		dragleave: function(e) { dropZone.removeClass('hover') ; eventCancel(e) },
//		dragenter: eventCancel,
//		dragexit : eventCancel,
//		drop: function(e){
//			eventCancel(e);
//
//			if (e.originalEvent.dataTransfer) {
//				if (e.originalEvent.dataTransfer.files.length) {
//
//					
//
//					/*UPLOAD FILES HERE*/
//					upload(e.originalEvent.dataTransfer.files);
//				}   
//			}
//			return false;
//		}
//	});
//
//	function upload(files) {
//		alert('Upload '+files.length+' File(s).');
//	}
//}