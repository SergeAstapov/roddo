<?php

namespace Roddo\EstimateBundle\Repository;

use Doctrine\ORM\EntityRepository;

class EstimateRepository extends EntityRepository
{
	public function findAllByAuthor($user_id) {
		$query = $this->createQueryBuilder('e')
			->where('e.user = :user_id')
			->setParameter('user_id', $user_id)
			->orderBy('e.created', 'DESC')
			->getQuery()
		;
		return $query->getResult();
	}
}