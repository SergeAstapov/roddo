<?php

namespace Roddo\EstimateBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;

class EstimateEmail
{
	/**
	 * @Assert\NotBlank(message="Please specify receiver email address.")
	 * @Assert\MaxLength(255)
	 */
	protected $email;

	/**
	 * @Assert\NotBlank(message="Message can not be sent without subject.")
	 * @Assert\MaxLength(255)
	 */
	protected $subject;

	protected $body;

	public function getEmail() {
		return $this->email;
	}

	public function setEmail($email) {
		$this->email = $email;
	}

	public function getSubject() {
		return $this->subject;
	}

	public function setSubject($subject) {
		$this->subject = $subject;
	}

	public function getBody() {
		return $this->body;
	}

	public function setBody($body) {
		$this->body = $body;
	}
}