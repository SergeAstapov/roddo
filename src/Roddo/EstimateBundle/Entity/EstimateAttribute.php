<?php
namespace Roddo\EstimateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext,
	Symfony\Component\HttpFoundation\File\File,
	Symfony\Component\HttpFoundation\File\UploadedFile,
	Symfony\Component\HttpKernel\Util\Filesystem;

use Roddo\EstimateBundle\Entity\Estimate;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="estimate_attribute")
 * @Assert\Callback(methods={"checkAttributeValue"})
 */
class EstimateAttribute
{
	/**
	 * @var \Roddo\EstimateBundle\Entity\Estimate
	 * 
	 * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Estimate", inversedBy="attributes")
	 * @Assert\Type(type="Roddo\EstimateBundle\Entity\Estimate")
     */
    private $estimate;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="string", length=255)
	 */
	private $attribute;

	/**
	 * @ORM\Column(type="text")
	 * @Assert\Date(groups="attribute_date")
	 * @Assert\Image(groups="attribute_logo", maxSize="1M")
	 */
	private $value;
	
	static public function getAllowedAttributes() {
		return array(
			'client_name',
			'client_company',
			'city',
			'date',
			'conditions',
			'author_name',
			'author_phone',
			'author_company',
			'author_about',
			'logo',
		);		
	}

    /**
     * Set attribute
     *
     * @param string $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * Get attribute
     *
     * @return string 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set value
     *
     * @param text $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return text 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set estimate
     *
     * @param \Roddo\EstimateBundle\Entity\Estimate $estimate
     */
    public function setEstimate(Estimate $estimate)
    {
        $this->estimate = $estimate;
    }

    /**
     * Get estimate
     *
     * @return \Roddo\EstimateBundle\Entity\Estimate
     */
    public function getEstimate()
    {
        return $this->estimate;
    }

	/**
	 * Validate attribute value depending on attribute type.
	 * 
	 * @param ExecutionContext $context
	 * @return void
	 */
	public function checkAttributeValue(ExecutionContext $context)
	{
		switch ($this->attribute) {
			case 'date' :
				$context
					->getGraphWalker()
					->walkReference($this, 'attribute_date', $context->getPropertyPath(), true);
				break;

			case 'logo' :
				$context
					->getGraphWalker()
					->walkReference($this, 'attribute_logo', $context->getPropertyPath(), true);
				break;
		}
	}

	/**
	 * @ORM\PostLoad
	 */
	public function postLoad()
	{
		if ('logo' == $this->attribute && null !== $this->value) {
			// Store previous image for case when new images is uploaded and
			// we have to remove prevous file from filesystem.
			$this->prevFile = new File($this->getAbsolutePath(), false);
			$this->prevValue = $this->value;
		}
	}

	/**
	 * @ORM\PreUpdate
	 * @ORM\PrePersist
	 */
	public function prePersist()
	{
		if ('logo' == $this->attribute) {
			if (null !== $this->value && $this->value instanceof UploadedFile) {
				$this->file  = $this->value;
				$this->value = $this->getSanitizedFilename($this->file);
			}
			else if (null === $this->value && !empty($this->prevValue)) {
				$this->value = $this->prevValue;
			}
		}
	}

	/**
	 * @ORM\PostPersist()
	 * @ORM\PostUpdate()
	 */
	public function upload()
	{
		if ('logo' == $this->attribute && !empty($this->file)) {
			if (!empty($this->prevFile) && $this->prevFile->isFile()) {
				$filesystem = new Filesystem();
				$filesystem->remove($this->prevFile);
			}

			// if there is an error when moving the file, an exception will
			// be automatically thrown by move(). This will properly prevent
			// the entity from being persisted to the database on error
			$this->file->move($this->getUploadRootDir(), $this->value);
		}
	}

	/**
	 * @ORM\PostRemove()
	 */
	public function removeUpload()
	{
		if ('logo' == $this->attribute && null !== $this->value) {
			$file = new File($this->getAbsolutePath());
			if ($file->isFile()) {
				$filesystem = new Filesystem();
				$filesystem->remove($this->prevFile);
			}
		}
	}

	/**
	 * 
	 * @return string
	 */
	private function getAbsolutePath()
    {
        return null === $this->value ? null : $this->getUploadRootDir() . '/' . basename($this->value);
    }

	/**
	 * 
	 * @return string
	 */
	private function getWebPath()
	{
		return null === $this->value ? null : $this->getUploadDir() .'/'. $this->value;
	}

	/**
	 * 
	 * @return string
	 */
	private function getUploadRootDir()
	{
		// the absolute directory path where uploaded documents should be saved
		return __DIR__ . '/../../../../web/' . $this->getUploadDir();
	}

	/**
	 * 
	 * @return string
	 */
	private function getUploadDir()
	{
		// get rid of the __DIR__ so it doesn't screw when displaying uploaded doc/image in the view.
		return 'media/estimate';
	}

	private function getSanitizedFilename(UploadedFile $file) {
		$whitelist = array('jpg jpeg gif png txt html doc xls pdf ppt pps odt ods odp');

//		$original = $file->getFilename();
		$filename = $file->getClientOriginalName();

		// Split the filename up by periods. The first part becomes the basename
		// the last part the final extension.
		$filename_parts = explode('.', $filename);
		$new_filename = array_shift($filename_parts); // Remove file basename.
		$final_extension = array_pop($filename_parts); // Remove final extension.

		// Loop through the middle parts of the name and add an underscore to the
		// end of each section that could be a file extension but isn't in the list
		// of allowed extensions.
		foreach ($filename_parts as $filename_part) {
			$new_filename .= '.' . $filename_part;
			if (!in_array($filename_part, $whitelist) && preg_match("/^[a-zA-Z]{2,5}\d?$/", $filename_part)) {
				$new_filename .= '_';
			}
		}
		$filename = $new_filename . '.' . $final_extension;

		if (preg_match('/\.(php|pl|py|cgi|asp|js)(\.|$)/i', $filename) && (substr($filename, -4) != '.txt')) {
			$filename .= '.txt';
		}

		return $this->checkFilename($filename);
	}

	private function checkFilename($basename) {
		// Strip control characters (ASCII value < 32). Though these are allowed in
		// some filesystems, not many applications handle them well.
		$basename  = preg_replace('/[\x00-\x1F]/u', '_', $basename);
		$directory = $this->getUploadDir();

		if (substr(PHP_OS, 0, 3) == 'WIN') {
			// These characters are not allowed in Windows filenames
			$basename = str_replace(array(':', '*', '?', '"', '<', '>', '|'), '_', $basename);
		}

		// A URI or path may already have a trailing slash or look like "public://".
		if (substr($directory, -1) == '/') {
			$separator = '';
		}
		else {
			$separator = '/';
		}

		$destination = $directory . $separator . $basename;

		if (file_exists($destination)) {
			// Destination file already exists, generate an alternative.
			$pos = strrpos($basename, '.');
			if ($pos !== FALSE) {
				$name = substr($basename, 0, $pos);
				$ext = substr($basename, $pos);
			}
			else {
				$name = $basename;
				$ext = '';
			}

			$counter = 0;
			do {
				$destination = $directory . $separator . $name . '_' . $counter++ . $ext;
			} while (file_exists($destination));
		}

		return $destination;
	}
}