<?php
namespace Roddo\EstimateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;

use Roddo\EstimateBundle\Entity\User;
use Roddo\EstimateBundle\Entity\Client;
use Roddo\EstimateBundle\Entity\EstimateItem;
use Roddo\EstimateBundle\Entity\EstimateAttribute;

// @ORM\Table(indexes={@ORM\Index(name="email_address_idx", columns={"email_address"})})
/**
 * @ORM\Entity(repositoryClass="Roddo\EstimateBundle\Repository\EstimateRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name = "estimate")
 */
class Estimate
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="estimate")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

	/**
     * @ORM\ManyToOne(targetEntity="Client", inversedBy="estimate")
     * @ORM\JoinColumn(name="client_id", referencedColumnName="id")
     */
    private $client;

	/**
     * @var datetime $created
     *
     * @ORM\Column(type="datetime")
     */
    private $created;

	/**
     * @var datetime $updated
	 * @ORM\Column(type="datetime")
	 */
	private $updated;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank(message="Estimate title value should not be blank")
	 */
	private $title;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $description;

	/**
	 * @ORM\Column(type="float")
	 */
	private $total;

	/**
	 * @ORM\Column(type="float")
	 */
	private $tax;

	/**
	 * @ORM\Column(type="string", length=32)
	 */
	private $currency;

	/**
     * @ORM\OneToMany(targetEntity="EstimateItem", mappedBy="estimate", cascade={"all"}, orphanRemoval=true)
     */
    private $items;

	/**
     * @ORM\OneToMany(
	 *		targetEntity  = "EstimateAttribute",
	 *		mappedBy      = "estimate",
	 *		cascade       = {"ALL"},
	 *		indexBy       = "attribute"
	 * )
     */
    private $attributes;

	/**
	 * Setup default values for a new estimate.
	 */
    public function __construct()
    {
		$this->items = new ArrayCollection();

		$this->attributes = new ArrayCollection();
		$this->loadAttributes();

		$this->setCreated(new \DateTime());
    }

	/**
	 * Since Estimate is part os composite key in EstimateAttribute entity
	 * we define this magic method because Doctrine ORM throws exception when
	 * Estimate entity is removed and UnitOfWork should get the key if to
	 * remove EstimateAttribute entity in DB.
	 */
	public function __toString() {
		return '' . $this->id;
	}

	/**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param \Roddo\EstimateBundle\Entity\User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return \Roddo\EstimateBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set client
     *
     * @param \Roddo\EstimateBundle\Entity\Client $client
     */
    public function setClient(Client $client)
    {
        $this->client = $client;
    }

    /**
     * Get client
     *
     * @return \Roddo\EstimateBundle\Entity\Client
     */
    public function getClient()
    {
        return $this->client;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set total
     *
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set tax
     *
     * @param float $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * Get tax
     *
     * @return float 
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set currency
     *
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

	/**
	 * Create new empty estimate item that can be used as 'placeholder' for new
	 * values.
	 */
	public function addEmptyItem() {
		$maxDelta = 0;
		$this->items->map(
			function(EstimateItem $item) use (&$maxDelta) {
				$maxDelta = max($maxDelta, $item->getDelta());
			}
		);

		$item = new EstimateItem();
		$item->setEstimate($this);
		$item->setDelta($maxDelta + 1);

		$this->addItem($item);
	}

    /**
     * Add items
     *
     * @param \Roddo\EstimateBundle\Entity\EstimateItem $item
     */
    public function addItem(EstimateItem $item)
    {
		$this->items->set($item->getDelta(), $item);
    }

    /**
     * Set items
     *
     * @param \Doctrine\Common\Collections\Collection $items
     */
	public function setItems(Collection $items) {
		foreach ($items as $item) {
			$item->setEstimate($this);

			if ($item->isEmpty()) {
				$items->removeElement($item);
			}
		}

		$this->items = $items;
	}

    /**
     * Get items
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add attribute
     *
     * @param \Roddo\EstimateBundle\Entity\EstimateAttribute $attribute
     */
    public function addAttribute(EstimateAttribute $attribute)
    {
		$this->attributes->set($attribute->getAttribute(), $attribute);
    }

    /**
     * Set attributes.
     *
     * @param \Doctrine\Common\Collections\Collection $attributes
     */
	public function setAttributes(Collection $attributes) {
		foreach ($attributes as $attribute) {
			$attribute->setEstimate($this);

			if (null === $attribute->getValue() || '' === $attribute->getValue()) {
				$attributes->remove($attribute->getAttribute());
			}
		}

		$this->attributes = $attributes;
	}

    /**
     * Get attributes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * Get specific attribute.
     *
     * @return \Roddo\EstimateBundle\Entity\EstimateAttribute
     */
    public function getAttribute($name)
    {
		return $this->attributes->get($name);
    }

	/**
	 * Set up all possible attributes to be presented in Estimate object.
	 * 
	 * @ORM\PostLoad
	 */
	public function loadAttributes() {
		foreach (EstimateAttribute::getAllowedAttributes() as $attribute_name) {
			if (null == $this->attributes->get($attribute_name)) {
				$attribute = new EstimateAttribute();
				$attribute->setAttribute($attribute_name);
				$attribute->setEstimate($this);

				$this->addAttribute($attribute);
			}
		}
	}

//	/**
//	 * Presave client.
//	 * 
//	 * @ORM\PrePersist
//	 */
//	public function __persistClient()
//	{
//		$client = $this->getClient();
//		if (!$client->getId()) {
//			$user = $this->getUser();
//			if (!empty($user)) {
//				$client->user_id = $client->getId();
//			}
//		}
//	}

	/**
	 * @ORM\PreUpdate
	 * @ORM\PrePersist
	 */
	public function calcUpdatedDate() {
		$this->setUpdated(new \DateTime());
	}

	/**
	 * Set estimate total.
	 * 
	 * @ORM\PreUpdate
	 * @ORM\PrePersist
	 */
	public function calcTotal() {
		$total = 0;
		$items = $this->getItems();

		if ($items->count()) {
			$items->map(
				function($item) use (&$total) {
					$total += $item->getQuantity() * $item->getPrice();
				}
			);
		}
		$this->setTotal($total);

		if (is_null($this->tax)) {
			$this->setTax(0);
		}
	}

	/**
	 * Order items within Estimate object.
	 * 
	 * @ORM\PreUpdate
	 * @ORM\PrePersist
	 */
	public function sortItems() {
		$items = $this->getItems();

		if ($items->count()) {
			$iterator = $items->getIterator();
			$iterator->uasort(function ($a, $b) {
				if ($a->getDelta() == $b->getDelta()) {
					return 0;
				}
				return ($a->getDelta() < $b->getDelta()) ? -1 : 1;
			});
		}
	}
}