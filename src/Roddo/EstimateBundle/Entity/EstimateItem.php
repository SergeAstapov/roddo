<?php
namespace Roddo\EstimateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

use Roddo\EstimateBundle\Entity\Estimate;

/**
 * @ORM\Entity
 * @ORM\Table(name="estimate_item")
 */
class EstimateItem
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $item_id;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $delta;

	/**
     * @ORM\ManyToOne(targetEntity="Estimate")
     * @ORM\JoinColumn(name="estimate_id", referencedColumnName="id")
	 * @Assert\Type(type="Roddo\EstimateBundle\Entity\Estimate")
     */
    protected $estimate;

	/**
	 * @ORM\Column(type="text")
	 */
	protected $description;

	/**
	 * @ORM\Column(type="integer")
	 */
	protected $quantity;

	/**
	 * @ORM\Column(type="float")
	 */
	protected $price;

//	/**
//	 * 
//	 * @param integer $delta
//	 * @param text $description
//	 * @param integer $quantity
//	 * @param float $price
//	 * @param \Roddo\EstimateBundle\Entity\Estimate $estimate
//	 */
//	public function __construct($delta = 0, $description = '', $quantity = '', $price = '', Estimate $estimate = NULL)
//    {
//        $this->delta       = $delta;
//		$this->description = $description;
//		$this->quantity    = $quantity;
//		$this->price       = $price;
//		$this->estimate    = $estimate;
//    }

    /**
     * Get item_id
     *
     * @return integer 
     */
    public function getItemId()
    {
        return $this->item_id;
    }

    /**
     * Set delta
     *
     * @param integer $delta
     */
    public function setDelta($delta)
    {
        $this->delta = $delta;
    }

    /**
     * Get delta
     *
     * @return integer 
     */
    public function getDelta()
    {
        return $this->delta;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set quantity
     *
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * Get quantity
     *
     * @return float 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set price
     *
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set estimate
     *
     * @param \Roddo\EstimateBundle\Entity\Estimate $estimate
     */
    public function setEstimate(Estimate $estimate)
    {
        $this->estimate = $estimate;
    }

    /**
     * Get estimate
     *
     * @return \Roddo\EstimateBundle\Entity\Estimate
     */
    public function getEstimate()
    {
        return $this->estimate;
    }

	/**
	 * Check whether the Estimate Item is empty and should be skipped
	 * while savind Estimate.
	 * 
	 * @return bool
	 */
	public function isEmpty() {
		return
			$this->getPrice() <= 0 ||
			$this->getQuantity() <= 0 ||
			trim($this->getDescription()) == '';
	}
}