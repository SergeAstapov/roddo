<?php
namespace Roddo\EstimateBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Symfony\Component\Validator\Constraints as Assert;

use Roddo\EstimateBundle\Entity\User;
use Roddo\EstimateBundle\Entity\Estimate;

/**
 * @ORM\Entity(repositoryClass="Roddo\EstimateBundle\Repository\ClientRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="client")
 */
class Client
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	protected $id;

	/**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="clients", fetch="LAZY")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;


	/**
     * @ORM\OneToMany(targetEntity="Estimate", mappedBy="client", fetch="LAZY")
     */
    protected $estimates;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\NotBlank(message="Client name value should not be blank")
	 */
	protected $name;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\NotBlank(message="Client company value should not be blank")
	 */
	protected $company_name;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\Email(
     *		message = "The email '{{ value }}' is not a valid email.",
     *		checkMX = true
     * )
	 */
	protected $mail;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $phone;

    public function __construct()
    {
        $this->estimates = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set company_name
     *
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;
    }

    /**
     * Get company_name
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set mail
     *
     * @param string $mail
     */
    public function setMail($mail)
    {
        $this->mail = $mail;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set phone
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set user
     *
     * @param \Roddo\EstimateBundle\Entity\User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return \Roddo\EstimateBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Add estimate
     *
     * @param \Roddo\EstimateBundle\Entity\Estimate $estimate
     */
    public function addEstimate(Estimate $estimate)
    {
        $this->estimates[] = $estimate;
    }

    /**
     * Get estimates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEstimates()
    {
        return $this->estimates;
    }
}