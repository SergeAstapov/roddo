<?php

namespace Roddo\EstimateBundle\Entity;

use FOS\UserBundle\Entity\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;

use Roddo\EstimateBundle\Entity\Client,
	Roddo\EstimateBundle\Entity\Estimate;

/**
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\MaxLength(255)
	 */
	protected $fullname;

	/**
	 * @ORM\Column(type="string", length=32, nullable=true)
	 * @Assert\MaxLength(32)
	 */
	protected $phone;

	/**
	 * @ORM\Column(type="string", length=255, nullable=true)
	 * @Assert\MaxLength(255)
	 */
	protected $company_name;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $company_about;

    /**
     * @ORM\Column(type="integer")
     */
	protected $facebook_id = 0;

    /**
     * @ORM\Column(type="integer")
     */
	protected $twitter_id = 0;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
	protected $linkedin_id = 0;

    /**
     * @ORM\Column(type="integer")
     */
	protected $google_id = 0;

	/**
     * @ORM\OneToMany(targetEntity="Estimate", mappedBy="user")
     */
    protected $estimates;

	/**
     * @ORM\OneToMany(targetEntity="Client", mappedBy="user")
     */
    protected $clients;

    public function __construct()
    {
        parent::__construct();
        // your own logic
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add estimates
     *
     * @param \Roddo\EstimateBundle\Entity\Estimate $estimate
     */
    public function addEstimate(Estimate $estimate)
    {
        $this->estimates[] = $estimate;
    }

    /**
     * Get estimates
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getEstimates()
    {
        return $this->estimates;
    }

    /**
     * Add clients
     *
     * @param \Roddo\ClientBundle\Entity\Client $clients
     */
    public function addClient(Client $client)
    {
        $this->clients[] = $client;
    }

    /**
     * Get clients
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getClients()
    {
        return $this->clients;
    }

    /**
     * Set fullname
     *
     * @param string $fullname
     */
    public function setFullname($fullname)
    {
        $this->fullname = $fullname;
    }

    /**
     * Get fullname
     *
     * @return string 
     */
    public function getFullname()
    {
        return $this->fullname;
    }

    /**
     * Set phone
     *
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * Get phone
     *
     * @return string 
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set company_name
     *
     * @param string $companyName
     */
    public function setCompanyName($companyName)
    {
        $this->company_name = $companyName;
    }

    /**
     * Get company_name
     *
     * @return string 
     */
    public function getCompanyName()
    {
        return $this->company_name;
    }

    /**
     * Set company_about
     *
     * @param text $companyAbout
     */
    public function setCompanyAbout($companyAbout)
    {
        $this->company_about = $companyAbout;
    }

    /**
     * Get company_about
     *
     * @return text 
     */
    public function getCompanyAbout()
    {
        return $this->company_about;
    }

    /**
     * Set facebook_id
     *
     * @param integer $facebook_id
     */
    public function setFacebook_id($facebook_id)
    {
        $this->facebook_id = $facebook_id;
    }

    /**
     * Get facebook_id
     *
     * @return integer 
     */
    public function getFacebookId()
    {
        return $this->facebook_id;
    }

    /**
     * Set twitter_id
     *
     * @param integer $twitterId
     */
    public function setTwitter_id($twitter_id)
    {
        $this->twitter_id = $twitter_id;
    }

    /**
     * Get twitter_id
     *
     * @return integer 
     */
    public function getTwitter_id()
    {
        return $this->twitter_id;
    }

    /**
     * Set linkedin_id
     *
     * @param string $linkedinId
     */
    public function setLinkedin_id($linkedin_id)
    {
        $this->linkedin_id = $linkedin_id;
    }

    /**
     * Get linkedin_id
     *
     * @return string 
     */
    public function getLinkedinId()
    {
        return $this->linkedin_id;
    }

    /**
     * Set google_id
     *
     * @param integer $googleId
     */
    public function setGoogleId($googleId)
    {
        $this->google_id = $googleId;
    }

    /**
     * Get google_id
     *
     * @return integer 
     */
    public function getGoogleId()
    {
        return $this->google_id;
    }
}