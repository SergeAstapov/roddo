<?php
namespace Roddo\EstimateBundle\Menu;

use Knp\Menu\FactoryInterface,
	Knp\Menu\ItemInterface,
	Symfony\Component\DependencyInjection\ContainerAware;

/**
 * Menu builder.
 */
class Builder extends ContainerAware
{
	/**
	 * Builds frontend main menu.
	 *
	 * @param FactoryInterface $factory
	 * @param array $options
	 *
	 * @return ItemInterface
	 */
	public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', array(
			'childrenAttributes' => array(
				'class' => 'menu main-menu',
				'uri'   => $this->container->get('request')->getRequestUri(),
			)
		));
//		$menu->setCurrentUri($this->container->get('request')->getRequestUri());
		$menu->setCurrent(null);

		$menu->addChild('eastimate_create', array(
			'route' => 'estimate_create',
			'label' => 'Create estimate',
		));
		$menu['eastimate_create']->setAttribute('class', 'create');

        $menu->addChild('frontpage', array(
			'route' => 'frontpage',
			'label' => 'Home'
		));
        $menu->addChild('eastimate_list', array(
			'route' => 'estimate_list',
			'label' => 'Estimages'
		));
		$menu->addChild('clients', array(
			'uri'   => '#',
			'label' => 'Clients'
		));
		$menu->addChild('user_settings', array(
			'route' => 'user_settings',
			'label' => 'Settings'
		));

		// Hide links.
		if (!$this->hasRole('IS_AUTHENTICATED_FULLY')) {
			$menu['eastimate_list']->setDisplay(FALSE);
			$menu['clients'] ->setDisplay(FALSE);
			$menu['user_settings']->setDisplay(FALSE);
		}

		foreach($menu as $key => $item) {
			$item->setExtra('routes', array(
				'routes' => $key
			));
		}

        return $menu;
    }

	/**
	 * Builds frontend main menu.
	 *
	 * @param FactoryInterface $factory
	 * @param array $options
	 *
	 * @return ItemInterface
	 */
	public function userMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root', array(
			'childrenAttributes' => array(
				'class' => 'menu user-menu',
				'uri'   => $this->container->get('request')->getRequestUri(),
			)
		));

		$menu->addChild('help', array(
			'route' => 'page_help',
			'label' => 'Help'
		));
		$menu->addChild('login',  array(
			'route' => 'hwi_oauth_connect',
			'label' => 'Login'
		));
        $menu->addChild('logout', array(
			'route' => 'fos_user_security_logout',
			'label' => 'Logout'
		));

		$menu[
			$this->hasRole('IS_AUTHENTICATED_FULLY') ? 'login' : 'logout'
		]->setDisplay(FALSE);

        return $menu;
    }

	private function hasRole($role) {
		return $this->container->get('security.context')->isGranted($role);
	}
}