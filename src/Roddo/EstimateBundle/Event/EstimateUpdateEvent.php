<?php
namespace Roddo\EstimateBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Roddo\EstimateBundle\Entity\Estimate;

class EstimateUpdateEvent extends Event
{
    protected $estimate;

	/**
	 * @param \Roddo\EstimateBundle\Entity\Estimate
	 */
    public function __construct(Estimate $estimate)
    {
        $this->estimate = $estimate;
    }

	/**
	 * @return \Roddo\EstimateBundle\Entity\Estimate
	 */
    public function getEstimate()
    {
        return $this->estimate;
    }
}