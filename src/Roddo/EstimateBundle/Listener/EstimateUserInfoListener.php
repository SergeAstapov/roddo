<?php
namespace Roddo\EstimateBundle\Listener;

use Roddo\EstimateBundle\Entity\Estimate,
	Roddo\EstimateBundle\Event\EstimateUpdateEvent;

use Symfony\Component\DependencyInjection\ContainerInterface;


/**
 * Listener service that saves user contact information when estimate has been
 * created by user.
 */
class EstimateUserInfoListener
{
	/**
	 * Symfony request container.
	 * 
	 * @var \Symfony\Component\DependencyInjection\ContainerInterface
	 */
	private $container;

	/**
	 * Mapping between User object properties and Estimate attributes.
	 * 
	 * @var array 
	 */
	private $mapping = array(
		'Fullname'     => 'author_name',
		'Phone'        => 'author_phone',
		'CompanyName'  => 'author_company',
		'CompanyAbout' => 'author_about',
	);

	/**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	/**
	 * Check whether user profile lacks some information that
	 * can be set from estimate.
	 * Fired on POST_UPDATE event.
	 * 
	 * @param \Roddo\EstimateBundle\Event\EstimateUpdateEvent $event
	 */
	public function updateUserInfo(EstimateUpdateEvent $event) {
		/**
		 * @var \Roddo\EstimateBundle\Entity\Estimate Estimate is being
		 * created or updated
		 */
		$estimate = $event->getEstimate();


		if ($this->userIsLoggedIn() && $this->userIsEstimateOwner($estimate)) {
			$flag = FALSE;
			$user = $this->container
				->get('security.context')
				->getToken()->getUser();

			foreach ($this->mapping as $userProperty => $attributeName) {
				$userGetter = 'get' . $userProperty;
				$userSetter = 'set' . $userProperty;
				$attribute  = $estimate->getAttribute($attributeName);

				if (NULL == $user->{$userGetter}() &&
					NULL != $attribute && NULL != $attribute->getValue())
				{
					$user->{$userSetter}($attribute->getValue());
					$flag = TRUE;
				}
			}


			// Are there some updates in user object?
			if ($flag) {
				$this->container
					->get('fos_user.user_manager')
					->updateUser($user);
			}
		}
	}

	/**
	 * Check whether current user is logged in.
	 * 
	 * @return bool Returns TRUE when user is logged in and FALSE when this is 
	 * anonymous visitor.
	 */
	private function userIsLoggedIn()
	{
		return $this->container->get('security.context')->isGranted('ROLE_USER');
	}

	/**
	 * Check whether cuccenr user is estimate owner.
	 * 
	 * @param \Roddo\EstimateBundle\Entity\Estimate $estimate
	 * @return bool Return TRUE if user is logged in and estimate owner.
	 */
	private function userIsEstimateOwner(Estimate $estimate)
	{
		$user = $this->container
			->get('security.context')
			->getToken()->getUser();

		return NULL !== $estimate->getUser() && $estimate->getUser()->getId() == $user->getId();
	}
}