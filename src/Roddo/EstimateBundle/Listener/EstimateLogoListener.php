<?php
namespace Roddo\EstimateBundle\Listener;

use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Events;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Roddo\EstimateBundle\Entity\Estimate;

/**
 * Listener service that move uploaded logo files from temp directory.
 */
class EstimateLogoListener
{
	private $request;
	private $container;

	/**
	 * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
	 */
	public function __construct(ContainerInterface $container)
	{
		$this->container = $container;
	}

	/**
	 * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
	 */
	public function prePersist(LifecycleEventArgs $args) {
		$this->preUpload($args->getEntity());
	}

	/**
	 * @param \Doctrine\ORM\Event\LifecycleEventArgs $args
	 */
	public function preUpdate(LifecycleEventArgs $args) {
		$this->preUpload($args->getEntity());
	}

	private function preUpload($entity) {
		if (!($entity instanceof EstimateAttribute) || 'logo' != $entity->getAttribute()) {
			return;
		}

		$file = $entity->getValue();
		print __FILE__ .':'. __LINE__;
		var_dump($file);
		exit;

		if (null !== $logoAttribute) {
			$file = $logoAttribute->getValue();

			if (null !== $file) {
				$prefix = 'estm_' . $this->container->getParameter('kernel.environment');
				$entity->logo_filename = uniqid($prefix) . '.' . $file->guessExtension();
			}
		}
	}

	/**
	 * @param Doctrine\ORM\Event\LifecycleEventArgs $args
	 */
	public function postPersist(LifecycleEventArgs $args) {
		$this->preUpload($args->getEntity());
	}

	/**
	 * @param Doctrine\ORM\Event\LifecycleEventArgs $args
	 */
	public function postUpdate(LifecycleEventArgs $args) {
		$this->upload($args->getEntity());
	}

	/**
	 * 
	 * @param Doctrine\ORM\Event\LifecycleEventArgs $args
	 */
	public function upload($entity)
	{
//		$entity = $args->getEntity();
//		if ($entity instanceof Estimate) {
//			$file = $entity->getAttribute('logo');
//
//			if (null !== $file) {
////				$upload_dir = $this->container->getParameter('roddo_estimate.upload_estimate_logo_dir');
//
//				// move takes the target directory and then the target filename to move to
//				$file->move($this->getUploadRootDir(), $file->guessExtension());
//				$entity->logo_filename = uniqid() . '.' . $file->guessExtension();
//			}
//		}
	}

	private function getUploadRootDir() {
		return implode('/', array(
			$this->request->getBasePath() . '/',
			$this->container->getParameter('roddo_estimate.upload_estimate_logo_dir')
		));
	}
//	private function upload(Estimate $entity) {
//		if (null === $this->file) {
//			return;
//		}
//
//		// we use the original file name here but you should
//		// sanitize it at least to avoid any security issues
//
//		// move takes the target directory and then the target filename to move to
//		$this->file->move($this->getUploadRootDir(), $this->file->getClientOriginalName());
//
//		// set the path property to the filename where you'ved saved the file
//		$this->path = $this->file->getClientOriginalName();
//
//		// clean up the file property as you won't need it anymore
//		$this->file = null;
//	}
}