<?php
namespace Roddo\EstimateBundle\Listener;

use Roddo\EstimateBundle\Entity\Client,
	Roddo\EstimateBundle\Entity\Estimate,
	Roddo\EstimateBundle\Event\EstimateUpdateEvent;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Listener service that saves user contact information when estimate has been
 * created by user.
 */
class EstimateClientListener
{
	private $container;

	/**
	 * Mapping between Client object properties and Estimate attributes.
	 * 
	 * @var array 
	 */
	private $mapping = array(
		'name'        => 'client_name',
		'CompanyName' => 'client_company',
//		'mail'        => 'author_company',
//		'phone'       => 'author_about',
	);

	/**
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
     */
	public function __construct(ContainerInterface $container) {
		$this->container = $container;
	}

	/**
	 * 
	 * @param \Roddo\EstimateBundle\Event\EstimateUpdateEvent $event
	 */
	public function onPreUpdate(EstimateUpdateEvent $event) {
		$estimate = $event->getEstimate();

		if ($this->userIsLoggedIn() && NULL == $estimate->getClient()) {
			$client = $this->createClientFromEstimate($estimate);

			$em = $this->container->get('doctrine')->getEntityManager();
			$em->persist($client);
			$em->flush();

			$estimate->setClient($client);
		}
	}

	/**
	 * Check whether cuccenr user is estimate owner.
	 * 
	 * @param \Roddo\EstimateBundle\Entity\Estimate $estimate
	 * @return \Roddo\EstimateBundle\Entity\Client
	 */
	private function createClientFromEstimate(Estimate $estimate)
	{
		$client = new Client();
		$client->setUser($estimate->getUser());

		foreach ($this->mapping as $clientProperty => $attributeName) {
			$clientSetter = 'set' . $clientProperty;
			$attribute = $estimate->getAttribute($attributeName);

			if (null != $attribute && null != $attribute->getValue())
			{
				$client->{$clientSetter}($attribute->getValue());
			}
			
		}

		return $client;
	}

	private function userIsLoggedIn() {
		return $this->container
			->get('security.context')
			->isGranted('ROLE_USER');
	}
}