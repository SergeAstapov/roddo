<?php
namespace Roddo\EstimatesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

use Gedmo\Mapping\Annotation as Gedmo;

use Symfony\Component\Validator\Constraints as Assert;

use Roddo\EstimatesBundle\Entity\User;
use Roddo\EstimatesBundle\Entity\EstimateItem;
use Roddo\EstimatesBundle\Entity\EstimateAttribute;

// @ORM\Table(indexes={@ORM\Index(name="email_address_idx", columns={"email_address"})})
/**
 * @ORM\Entity(repositoryClass="Roddo\EstimatesBundle\Repository\EstimateRepository")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Table(name="estimate")
 */
class Estimate
{
	/**
	 * @ORM\Id
	 * @ORM\Column(type="integer")
	 * @ORM\GeneratedValue(strategy="AUTO")
	 */
	private $id;

	/**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="estimate")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

	/**
     * @var datetime $created
     *
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(type="datetime")
     */
    private $created;

	/**
     * @var datetime $updated
     * @Gedmo\Timestampable(on="update")
	 * @ORM\Column(type="datetime")
	 */
	private $updated;

	/**
	 * @ORM\Column(type="string", length=255)
	 * @Assert\NotBlank(message="Estimate title value should not be blank")
	 */
	private $title;

	/**
	 * @ORM\Column(type="text", nullable=true)
	 */
	private $description = '';

	/**
	 * @ORM\Column(type="float")
	 */
	private $total;

	/**
	 * @ORM\Column(type="float")
	 */
	private $tax;

	/**
	 * @ORM\Column(type="string", length=255)
	 */
	private $currency;

	/**
     * @ORM\OneToMany(
	 *		targetEntity="EstimateItem",
	 *		mappedBy="estimate",
	 *		cascade={"all"},
	 *		orphanRemoval=true,
	 *		fetch="LAZY"
	 * )
     */
    private $items;

	/**
     * @ORM\OneToMany(
	 *		targetEntity  = "EstimateAttribute",
	 *		mappedBy      = "estimate",
	 *		cascade       = {"all"},
	 *		indexBy       = "attribute",
	 *		orphanRemoval = true,
	 *		fetch         = "LAZY"
	 * )
     */
    private $attributes;

	/**
	 * Setup default values for a new estimate.
	 */
    public function __construct()
    {
		// Setup estimate attributes.
		foreach (EstimateAttribute::getAllowedAttributes() as $name) {
			$this->addAttribute(new EstimateAttribute($name, '', $this));
		}

		// Setup estimate items.
		$this->addItem(new EstimateItem(0, '', '', '', $this));

		$this->setCurrency('sek');
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param Roddo\EstimatesBundle\Entity\User $user
     */
    public function setUser(\Roddo\EstimatesBundle\Entity\User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     *
     * @return Roddo\EstimatesBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set created
     *
     * @param datetime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * Get created
     *
     * @return datetime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set updated
     *
     * @param datetime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * Get updated
     *
     * @return datetime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set title
     *
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param text $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * Get description
     *
     * @return text 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set total
     *
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * Get total
     *
     * @return float 
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set tax
     *
     * @param float $tax
     */
    public function setTax($tax)
    {
        $this->tax = $tax;
    }

    /**
     * Get tax
     *
     * @return float 
     */
    public function getTax()
    {
        return $this->tax;
    }

    /**
     * Set currency
     *
     * @param string $currency
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;
    }

    /**
     * Get currency
     *
     * @return string 
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Add items
     *
     * @param EstimateItem $items
     */
    public function addItem(EstimateItem $item)
    {
		$this->items[$item->getDelta()] = $item;
    }

    /**
     * Set items
     *
     * @param Roddo\EstimatesBundle\Entity\EstimateItem $items
     */
	public function setItems($items) {
		foreach ($items as $delta => $item) {
			$item->setEstimate($this);

			if ($item->isEmpty()) {
				unset($items[$delta]);
			}
		}

		$this->items = $items;
	}

    /**
     * Get items
     *
     * @return Doctrine\Common\Collections\Collection
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * Add items
     *
     * @param Roddo\EstimatesBundle\Entity\EstimateItem $items
     */
    public function addEstimateItem(\Roddo\EstimatesBundle\Entity\EstimateItem $items)
    {
        $this->items[] = $items;
    }

    /**
     * Add attribute
     *
     * @param string $name
	 * @param mixed $value
     */
    public function addAttribute(EstimateAttribute $attribute)
    {
        $this->attributes[$attribute->getAttribute()] = $attribute;
    }

    /**
     * Set attributes.
     *
     * @param Roddo\EstimatesBundle\Entity\EstimateAttribute $attributes
     */
	public function setAttributes($attributes) {
		foreach ($attributes as $attribute) {
			$attribute->setEstimate($this);
		}

		$this->attributes = $attributes;
	}

    /**
     * Get attributes
     *
     * @return Doctrine\Common\Collections\Collection 
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

//	/**
//	 * Presave client.
//	 * 
//	 * @ORM\PrePersist
//	 */
//	public function __persistClient()
//	{
//		$client = $this->getClient();
//		if (!$client->getId()) {
//			$user = $this->getUser();
//			if (!empty($user)) {
//				$client->user_id = $client->getId();
//			}
//		}
//	}

	/**
	 * Order items within 
	 * 
	 * @ORM\PrePersist
	 */
	public function __setItemsDelta()
	{

		$total = 0;
		$items = $this->getItems();

		if (!empty($items)) {
			uasort($items, function ($a, $b) {
				if ($a->getDelta() == $b->getDelta()) {
					return 0;
				}
				return ($a->getDelta() < $b->getDelta()) ? -1 : 1;
			});

			$delta = 0;
			foreach ($items as $item) {
				$total += $item->getQuantity() * $item->getPrice();

				if (!$item->getDelta()) {
					$item->setDelta($delta);
				}

				$delta++;
			}
			$this->setItems($items);
		}

		$this->setTotal($total);

		if (!$this->getTax()) {
			$this->setTax(0);
		}
	}

	/**
	 * Order items within 
	 * 
	 * @ORM\PostLoad
	 */
	public function loadAttributes() {
		// Setup estimate attributes.
		foreach (EstimateAttribute::getAllowedAttributes() as $name) {
			if (!isset($this->attributes[$name])) {
				$this->addAttribute(new EstimateAttribute($name, '', $this));
			}
		}
	}
}