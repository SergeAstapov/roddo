<?php
namespace Roddo\EstimatesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\ExecutionContext;

use Roddo\EstimatesBundle\Entity\Estimate;

/**
 * @ORM\Entity
 * @ORM\Table(name="estimate_attribute")
 * @Assert\Callback(methods={"isAttributeValueValid"})
 */
class EstimateAttribute
{
	/**
	 * @var Estimate
	 * 
	 * @ORM\Id
     * @ORM\ManyToOne(targetEntity="Estimate", inversedBy="attributes")
	 * @ORM\JoinColumn(name="estimate_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
	 * @Assert\Type(type="Roddo\EstimatesBundle\Entity\Estimate")
     */
    private $estimate;

	/**
	 * @ORM\Id
	 * @ORM\Column(type="string", length=255)
	 */
	private $attribute;

	/**
	 * @ORM\Column(type="text")
	 * @Assert\Date(groups="attribute_date")
	 */
	private $value;

	public function __construct($name, $value, $estimate)
    {
        $this->attribute = $name;
		$this->value     = $value;
		$this->estimate  = $estimate;
    }
	
	static function getAllowedAttributes() {
		return array(
			'client_name',
			'client_company',
			'city',
			'date',
			'conditions',
			'author_name',
			'author_phone',
			'author_company',
			'author_about',
		);		
	}

    /**
     * Set attribute
     *
     * @param string $attribute
     */
    public function setAttribute($attribute)
    {
        $this->attribute = $attribute;
    }

    /**
     * Get attribute
     *
     * @return string 
     */
    public function getAttribute()
    {
        return $this->attribute;
    }

    /**
     * Set value
     *
     * @param text $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * Get value
     *
     * @return text 
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set estimate
     *
     * @param Estimate $estimate
     */
    public function setEstimate(Estimate $estimate)
    {
        $this->estimate = $estimate;
    }

    /**
     * Get estimate
     *
     * @return Estimate
     */
    public function getEstimate()
    {
        return $this->estimate;
    }

	/**
	 * Validate paypalEmailAddress if paypal is selected
	 * @param ExecutionContext $context
	 * @return void
	 */
	public function isAttributeValueValid(ExecutionContext $context)
	{
		switch ($this->attribute) {
			case 'date' :
				$context
					->getGraphWalker()
					->walkReference($this, 'attribute_date', $context->getPropertyPath(), true);
				break;
		}
	}
}