<?php
namespace Roddo\EstimatesBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EstimateClientType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$builder
			->add('name',    'text')
			->add('company', 'text')
		;
	}

	public function getName()
	{
		return 'estimate_client';
	}

	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class' => 'Roddo\EstimatesBundle\Entity\Client',
		);
	}
}