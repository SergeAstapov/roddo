<?php
namespace Roddo\EstimatesBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EstimateUserInfoType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$user = $builder->getData();
		if (!empty($user)) {
			$builder->setData($user->getUserInfo());
		}

		$builder
			->add('name',          'text',     array('property_path' => FALSE))
			->add('phone',         'text',     array('property_path' => FALSE))
			->add('company_name',  'text',     array('property_path' => FALSE))
			->add('company_notes', 'textarea', array('property_path' => FALSE))
		;
	}

	public function getName()
	{
		return 'estimate_user';
	}

	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class' => 'Roddo\EstimatesBundle\Entity\UserInfo',
		);
	}
}