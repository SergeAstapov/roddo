<?php

namespace Roddo\EstimatesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;
use Symfony\Component\Form\Extension\Core\ChoiceList\ArrayChoiceList;

class EstimateType extends AbstractType
{
    public function buildForm(FormBuilder $builder, array $options)
    {
		$estimate = $builder->getData();

        $builder
			->add('title', 'text')
			->add('description', 'textarea')
			->add('total', 'number')
			->add('tax',   'number')
			->add('currency', 'choice', array(
				'choices'  => array(
					'usd'   => 'USA dollars',
					'sek'   => 'Swedish krona',
					'euro'  => 'Euro'
				),
				'required' => true,
			))

			->add('items', 'collection', array(
				'type'         => new EstimateItemType(),
				'data'         => $estimate->getItems(),
				'allow_add'    => true,
				'allow_delete' => true,
				'prototype'    => true,
				// Post update
				'by_reference' => false,
			))

			->add('attributes', 'collection', array(
				'type'         => new EstimateAttributeType($estimate),
				'data'         => $estimate->getAttributes(),
				'allow_add'    => false,
				'allow_delete' => false,
				'prototype'    => false,
				// Post update
				'by_reference' => false,
			))
        ;
    }

    public function getName()
    {
        return 'roddo_estimate';
    }

	public function getDefaultOptions(array $options) {
		return array(
			'data_class' => 'Roddo\EstimatesBundle\Entity\Estimate',
		);
	}
}
