<?php
namespace Roddo\EstimatesBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilder;

class EstimateAttributeType extends AbstractType
{
	public function buildForm(FormBuilder $builder, array $options)
	{
		$options = array('required' => false);

		$item_type = 'text';
		switch ($builder->getName()) {
			case 'conditions' :
			case 'author_about' :
				$item_type = 'textarea';
				break;

			case 'date' :
//				$options['format'] = 'yyyy-MM-dd';
//				$options['widget'] = 'single_text';
				$options['invalid_message'] = 'You entered an invalid value - it should have the  YYYY-MM-DD format';
				break;
		}

		$builder->add('value', $item_type, $options);
	}

	public function getName()
	{
		return 'roddo_estimate_attribute';
	}

	public function getDefaultOptions(array $options)
	{
		return array(
			'data_class' => 'Roddo\EstimatesBundle\Entity\EstimateAttribute',
		);
	}
}