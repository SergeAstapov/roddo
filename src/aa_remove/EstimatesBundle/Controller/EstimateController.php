<?php

namespace Roddo\EstimatesBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

use Roddo\EstimatesBundle\Entity\Estimate;
use Roddo\EstimatesBundle\Form\EstimateType;

/**
 * Estimate controller.
 *
 */
class EstimateController extends Controller
{
    /**
     * Lists all Estimate entities.
     *
	 * @Template("RoddoEstimatesBundle::index.html.twig")
     */
    public function indexAction()
    {
		$user = $this->get('security.context')->getToken()->getUser();

        $em = $this->getDoctrine()->getEntityManager();

        $entities = $em->getRepository('RoddoEstimatesBundle:Estimate')->findAllByAuthor($user->getId());

        return array('entities' => $entities);
    }

    /**
     * Finds and displays a Estimate entity.
     *
	 * @Template("RoddoEstimatesBundle::show.html.twig")
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $estimate = $em->getRepository('RoddoEstimatesBundle:Estimate')->find($id);

        if (!$estimate) {
            throw $this->createNotFoundException('Unable to find Estimate entity.');
        }

		$attributes = array();
		foreach ($estimate->getAttributes() as $attribute) {
			$attributes[$attribute->getAttribute()] = $attribute->getValue();
		}

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'estimate'    => $estimate,
			'items'       => $estimate->getItems(),
			'attributes'  => $attributes,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Creates a new Estimate entity.
     *
	 * @Template("RoddoEstimatesBundle::create.html.twig")
     */
    public function createAction()
    {
		$em = $this->getDoctrine()->getEntityManager();

        $estimate = new Estimate();
        $request  = $this->getRequest();
        $form     = $this->createForm(new EstimateType(), $estimate, array('required' => FALSE));

		if ($request->getMethod() == 'POST') {
			$form->bindRequest($request);

			if ($form->isValid()) {
				$this->saveEstimate($estimate, $em);

				$this->get('session')->setFlash('notice', 'Estimate has been created. Hooray!');

				return $this->redirect($this->generateUrl('estimate_show', array('id' => $estimate->getId())));
			}
		}

        return array(
            'estimate' => $estimate,
            'form'     => $form->createView()
        );
    }

    /**
     * Displays a form to edit an existing Estimate entity.
     *
	 * @Template("RoddoEstimatesBundle::edit.html.twig")
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getEntityManager();

        $estimate = $em->getRepository('RoddoEstimatesBundle:Estimate')->find($id);

        if (!$estimate) {
            throw $this->createNotFoundException('Unable to find Estimate entity.');
        }

        $editForm   = $this->createForm(new EstimateType(), $estimate);
        $deleteForm = $this->createDeleteForm($id);

		$request = $this->getRequest();

		if ($request->getMethod() == 'POST') {
			$editForm->bindRequest($request);

			if ($editForm->isValid()) {
				$em->persist($estimate);
				$em->flush();

				$this->get('session')->setFlash('notice', 'Estimate has been updated.');

				return $this->redirect($this->generateUrl('estimate_show', array('id' => $id)));
			}
		}
        return array(
            'estimate'    => $estimate,
            'form'        => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Deletes a Estimate entity.
     *
     */
    public function deleteAction($id)
    {
        $form    = $this->createDeleteForm($id);
        $request = $this->getRequest();

        $form->bindRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getEntityManager();
            $entity = $em->getRepository('RoddoEstimatesBundle:Estimate')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Estimate entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('estimate'));
    }

    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

	private function saveEstimate($estimate, $em) {
//		$em->getConnection()->beginTransaction();
		try {
			/**
			 * Save attributes after Estimate Entity is saved, because we
			 * need estimate_id to be presented within each EstimateAttribute
			 * object since estimate_id is part of composite primary_key.
			 */
			$attributes = $estimate->getAttributes();
			$estimate->setAttributes(array());

			$em = $this->getDoctrine()->getEntityManager();
			$em->persist($estimate);
			$em->flush();

			foreach ($attributes as $attribute) {
				if ($attribute->getValue()) {
					$em->persist($attribute);
				}
			}
			$em->flush();
		}
		catch (\Exception $e) {
//            $em->getConnection()->rollback();
            $em->close();
            throw $e;
        }
	}
}
