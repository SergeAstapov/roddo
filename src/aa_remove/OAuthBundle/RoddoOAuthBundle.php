<?php

namespace Roddo\OAuthBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class RoddoOAuthBundle extends Bundle
{
	public function getParent()
	{
		return 'HWIOAuthBundle';
	}
}