(function ($) {

$(document).ready(function() {
	RoddoTextarea();
	RoddoDatePicker();
	new RoddoEstimateItems($('#form-estimate'), true);
});

function RoddoTextarea() {
	if ( $.fn.autosize ) {
		$('textarea').autosize();
	}
}

function RoddoDatePicker() {
	var $form = $('#form-estimate');
	if ( $.fn.datepicker && $form.length ) {
		$('input[name="'+ $form.attr('name') +'[attributes][date][value]"]', $form).datepicker({
			dateFormat: 'yy-mm-dd'
		});
	}
}

RoddoEstimateItems = function ($form, debug) {
	var self = this;

	self.$form = $form;
	self.debug = debug;
	self.name = $form.attr('name');

	self.$collection = $('#items-collection', $form);
	if (!self.$collection.length) return;

	self.prototype = self.$collection.data('prototype');
	self.$collection.removeAttr('data-prototype');

	self.maxDelta = self.$collection.children().length;	

	self.bindNewItem();
	self.bindCurrency();

	self.bindTotal();
};

RoddoEstimateItems.prototype.itemTotalTheme = function() {
	var self = this;

	return '<div class="form-item field-item-total">'+
		'<label>Total:</label>'+
		'<span class="estimate-currency">'+ self.viewCurrency() +'</span>'+
		'<span class="field-item-total-value">0.00</span>'+
	'</div>';
};

RoddoEstimateItems.prototype.viewCurrency = function() {
	var self = this;

	switch (self.currency) {
		case 'usd' : return '$';
		case 'sek' : return 'kr';
		case 'euro': return '&euro;';
	}
}

RoddoEstimateItems.prototype.itemAdd = function() {
	var self = this;

	self.maxDelta++;
	var form = $( self.prototype.replace(/\$\$name\$\$/g, self.maxDelta) );

	form.append( self.itemTotalTheme() );
	form.data('total', 0);

	self.$collection.append(form);
	self.itemDeltaUpdate();
};

RoddoEstimateItems.prototype.itemDeltaUpdate = function() {
	var self = this;

	var delta = 0;
	self.$collection.children().each(function() {
		$('input[name*="delta"]', this).val(delta);
		delta++;
	});
	self.maxDelta = delta;
};

RoddoEstimateItems.prototype.bindNewItem = function() {
	var self = this;

	$('a.jslink', self.$collection).live('click', function(e) {
		e.preventDefault();
		self.itemAdd();
	});

	$('input, textarea', self.$collection).live('keyup', function() {
		var $this  = $(this),
			$fItem = $this.parent('.form-item');
		if ( $.trim( $this.val() ).length && !$fItem.parent().nextAll().length) {
			self.itemAdd();
		}
	});
}

RoddoEstimateItems.prototype.bindCurrency = function() {
	var self = this;

	$('select[name="'+ self.name +'[currency]"]', self.$form).change(function() {
		self.currency = $( this ).val();
		$('.estimate-currency', self.$form).html( self.viewCurrency() );
	}).trigger('change');
}

RoddoEstimateItems.prototype.bindTotal = function() {
	var self = this;

	self.total = 0;
	self.tax   = 0;

	var $tax = $('input[name="'+ self.name +'[tax]"]', self.$form);

	$tax.keyup(function() {
		self.tax = castNum( $(this).val() ) * 0.01;

		$('#field-estimate-tax', self.$form).text( (self.total * self.tax).toFixed(2) );
	});

	var setEstimateTotal = function() {
		self.total = 0;

		self.$collection.children().each(function() {
			self.total += isNaN($( this ).data('total')) ? 0 : parseFloat($( this ).data('total'), 10);
		});

		$('#field-estimate-final', self.$form).text( (self.total + self.total * self.tax).toFixed(2) );
		$tax.keyup();
	};

	var selQ = 'input[name$="[quantity]"]',
		selP = 'input[name$="[price]"]';

	$(selQ +', '+ selP, self.$collection).live('keyup', function() {
		var $item = $( this ).parent('.form-item').parent();

		var total = ( castNum($(selQ, $item).val()) * castNum($(selP, $item).val(), 'float') ).toFixed(2);

		$('.field-item-total-value', $item).text(total);
		$item.data('total', total);

		setEstimateTotal();
	})
	.filter(':first').trigger('keyup');
}

function castNum(number, type) {
	type = type || 'int';
	if (isNaN(number) || !number.length) return 0;

	switch (type) {
		case 'int'  : return parseInt  (number, 10);
		case 'float': return parseFloat(number, 10);
	}
}



}(jQuery))