jQuery(document).ready(function() {
	var textarea = document.getElementsByTagName("textarea");
	if (textarea.length && jQuery.fn.autosize) {
		jQuery(textarea).autosize();
	}
});